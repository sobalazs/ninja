package conf;

import com.google.inject.Inject;
import ninja.lifecycle.Start;
import ninja.utils.NinjaProperties;
import utils.LoadTestData;

import javax.inject.Singleton;

/**
 * Add testdata to database.
 */
@Singleton
public class StartupAction {

    @Inject
    LoadTestData loadTestData;

    private NinjaProperties ninjaProperties;

    @Inject
    public StartupAction(NinjaProperties ninjaProperties) {
        this.ninjaProperties = ninjaProperties;
    }

    @Start(order = 100)
    public void generateDummyDataWhenInTest() {
        if (ninjaProperties.isDev()) {
            loadTestData.addTestUsers();
        }

    }

}
