package conf;

import controllers.ApplicationController;
import controllers.EventController;
import controllers.RouteController;
import controllers.UserController;
import ninja.AssetsController;
import ninja.Router;
import ninja.SecureFilter;
import ninja.application.ApplicationRoutes;
import utils.Path;

/**
 * Contains all the routes wwhere the app can provide response.
 */
public class Routes implements ApplicationRoutes {

    /**
     * Provide request routes.
     *
     * @param router Contains all information from requested route.
     */
    @Override
    public void init(Router router) {
        router.GET().route("/").with(ApplicationController::index);

        router.GET().route(Path.Web.EVENTS).with(EventController::events);
        router.GET().route(Path.Web.EVENT).with(EventController::event);
        router.GET().route(Path.Web.NEWEVENT).with(EventController::newEvent);
        router.POST().route(Path.Web.NEWEVENT).filters(SecureFilter.class).with(EventController::addEvent);
        router.GET().route(Path.Web.MODEVENT).filters(SecureFilter.class).with(EventController::event);
        router.POST().route(Path.Web.MODEVENT).filters(SecureFilter.class).with(EventController::modEvent);
        router.POST().route(Path.Web.DELEVENT).filters(SecureFilter.class).with(EventController::delEvent);
        router.GET().route(Path.Web.ADDROUTETOEVENT).filters(SecureFilter.class).with(EventController::addRouteToEventTemplate);
        router.POST().route(Path.Web.ADDROUTETOEVENT).filters(SecureFilter.class).with(EventController::addRouteToEvent);
        router.POST().route(Path.Web.DELROUTEFROMEVENT).filters(SecureFilter.class).with(EventController::delRouteFromEvent);
        router.POST().route(Path.Web.SUBSCRIBETOEVENT).filters(SecureFilter.class).with(EventController::subToEvent);

        router.GET().route(Path.Web.ROUTES).with(RouteController::routes);
        router.GET().route(Path.Web.ROUTE).with(RouteController::route);
        router.GET().route(Path.Web.NEWROUTE).with(RouteController::newRouteTemp);
        router.POST().route(Path.Web.NEWROUTE).filters(SecureFilter.class).with(RouteController::addRoute);
        router.GET().route(Path.Web.MODROUTE).filters(SecureFilter.class).with(RouteController::route);
        router.POST().route(Path.Web.MODROUTE).filters(SecureFilter.class).with(RouteController::modRoute);
        router.POST().route(Path.Web.DELROUTE).filters(SecureFilter.class).with(RouteController::delRoute);

        router.GET().route(Path.Web.USERS).filters(SecureFilter.class).with(UserController::users);
        router.GET().route(Path.Web.PROFILE).filters(SecureFilter.class).with(UserController::getProfileTemplate);
        router.GET().route(Path.Web.LOGIN).with(UserController::getLoginTemplate);
        router.POST().route(Path.Web.LOGIN).with(UserController::loginUser);
        router.GET().route(Path.Web.LOGOUT).with(UserController::logoutUser);


        ///////////////////////////////////////////////////////////////////////
        // Assets (pictures / javascript)
        ///////////////////////////////////////////////////////////////////////    
        router.GET().route("/assets/webjars/{fileName: .*}").with(AssetsController::serveWebJars);
        router.GET().route("/assets/{fileName: .*}").with(AssetsController::serveStatic);

        ///////////////////////////////////////////////////////////////////////
        // Index / Catchall shows index page
        ///////////////////////////////////////////////////////////////////////
        router.GET().route("/.*").with(ApplicationController::index);
    }
}
