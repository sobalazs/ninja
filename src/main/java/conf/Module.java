package conf;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

/**
 * Configure the app with all required modules add create bindings..
 */
@Singleton
public class Module extends AbstractModule {

    protected void configure() {
        bind(StartupAction.class);
    }

}