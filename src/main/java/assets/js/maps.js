var directionsDisplay;
var directionsService;
var startmarker = null;
var finishmarker = null;
var map;
var searchBox;
var input;
var markerlist = [];
var waypoints = [];
var modurl;
var dragable = true;
var element = 'map';
var yellow = 'http://maps.google.com/mapfiles/ms/icons/yellow.png';
var red = 'http://maps.google.com/mapfiles/ms/icons/red.png';
var green = 'http://maps.google.com/mapfiles/ms/icons/green.png';

function initMap() {

    map = new google.maps.Map(document.getElementById(element), {
        zoom: 7,
        center: {lat: 47.162494, lng: 19.503304}, // Hungary.
        mapTypeId: 'terrain'
    });
    if (markerlist.length != 0) {
        startmarker = new google.maps.Marker({
            position: new google.maps.LatLng({
                lat: markerlist[0][0],
                lng: markerlist[0][1]
            })
        });

        finishmarker = new google.maps.Marker({
            position: new google.maps.LatLng({
                lat: markerlist[markerlist.length - 1][0],
                lng: markerlist[markerlist.length - 1][1]
            }),
        });
    }


    directionsService = new google.maps.DirectionsService;

    directionsDisplay = new google.maps.DirectionsRenderer({
        draggable: dragable,
        map: map
    });

    if (modurl !== undefined) {
        directionsDisplay.setOptions({suppressMarkers: false});
    } else {
        directionsDisplay.setOptions({suppressMarkers: true});
    }

    if (startmarker == null || finishmarker == null) {
        //Searchbox Start
        // Create the search box and link it to the UI element.
        if (input == null) {
            input = document.getElementById("searchbox");
        }
        searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });
        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
        //Searchbox End
    }

    if (startmarker == null && finishmarker == null) {
        google.maps.event.addListener(map, "click", function (e) {
            addMarker(e);
        })
    }

    if (startmarker != null && finishmarker != null) {
        if (markerlist.length > 2) {
            for (var i = 1; i < markerlist.length - 1; i++) {
                waypoints[i - 1] = {
                    location: new google.maps.LatLng({lat: markerlist[i][0], lng: markerlist[i][1]}),
                    stopover: true
                }
            }
        }
        displayRoute(startmarker, finishmarker, directionsService, directionsDisplay, waypoints);
    }
}

function displayRoute(start, finish, service, display, waypoints) {
    service.route({
        origin: start.getPosition(),
        destination: finish.getPosition(),
        waypoints: waypoints,
        travelMode: 'DRIVING',
        optimizeWaypoints: true,
        avoidTolls: true
    }, function (response, status) {
        if (status === 'OK') {
            display.setDirections(response);
            if (modurl == undefined) {
                new google.maps.Marker({
                    position: new google.maps.LatLng({
                        lat: display.directions.routes[0].legs[0].start_location.lat(),
                        lng: display.directions.routes[0].legs[0].start_location.lng()
                    }),
                    draggable: dragable,
                    map: map,
                    icon: green
                });

                new google.maps.Marker({
                    position: new google.maps.LatLng({
                        lat: display.directions.routes[0].legs[display.directions.routes[0].legs.length - 1].end_location.lat(),
                        lng: display.directions.routes[0].legs[display.directions.routes[0].legs.length - 1].end_location.lng()
                    }),
                    draggable: dragable,
                    map: map,
                    icon: red
                });

                if (display.directions.routes[0].legs.length - 1 > 0) {
                    for (var l = 0; l < display.directions.routes[0].legs.length; l++) {
                        for (var ws = 0; ws < display.directions.routes[0].legs[l].via_waypoints.length; ws++) {
                            new google.maps.Marker({
                                position: new google.maps.LatLng({
                                    lat: display.directions.routes[0].legs[l].via_waypoints[ws].lat(),
                                    lng: display.directions.routes[0].legs[l].via_waypoints[ws].lng()
                                }),
                                draggable: dragable,
                                map: map,
                                icon: yellow
                            });
                        }
                        if (l != directionsDisplay.directions.routes[0].legs.length - 1) {
                            new google.maps.Marker({
                                position: new google.maps.LatLng({
                                    lat: display.directions.routes[0].legs[l].end_location.lat(),
                                    lng: display.directions.routes[0].legs[l].end_location.lng()
                                }),
                                draggable: dragable,
                                map: map,
                                icon: yellow
                            });
                        }
                    }
                } else {
                    waypoints = display.directions.routes[0].legs[0].via_waypoints;
                    for (var i = 0; i < waypoints.length; i++) {
                        new google.maps.Marker({
                            position: new google.maps.LatLng({
                                lat: waypoints[i].lat(),
                                lng: waypoints[i].lng()
                            }),
                            draggable: dragable,
                            map: map,
                            icon: yellow
                        });
                    }
                }
            }
        } else {
            alert('Nem jeleníthető meg útvonal!');
        }
    });
}

function addMarker(event) {
    var m = new google.maps.Marker({
        position: event.latLng,
        draggable: dragable,
        map: map,
        animation: google.maps.Animation.DROP
    });
    if (startmarker == null) {

        m.icon = 'http://maps.google.com/mapfiles/ms/icons/green.png';
        startmarker = m;
    } else {

        finishmarker = m;
        google.maps.event.clearListeners(map, "click", initMap());
    }
}

$(document).ready(function () {
    $("#save").click(function (e) {
        e.preventDefault();

        var dr = directionsDisplay.directions;
        if (dr == undefined) {
            alert("Nem menthető! Jelöljön ki legalább 2 pontot a térképen!");
        } else {
            var renderedRouteLegs = directionsDisplay.directions.routes[0].legs[0];

            var routelistJSON = [];
            var routeJSON = {};
            routeJSON.start = {
                lat: directionsDisplay.directions.routes[0].legs[0].start_location.lat(),
                lng: directionsDisplay.directions.routes[0].legs[0].start_location.lng(),
                address: directionsDisplay.directions.routes[0].legs[0].start_address
            };

            routeJSON.destination = {
                lat: directionsDisplay.directions.routes[0].legs[directionsDisplay.directions.routes[0].legs.length - 1].end_location.lat(),
                lng: directionsDisplay.directions.routes[0].legs[directionsDisplay.directions.routes[0].legs.length - 1].end_location.lng(),
                address: directionsDisplay.directions.routes[0].legs[directionsDisplay.directions.routes[0].legs.length - 1].end_address
            };
            var waypoints = [];
            var w = [];
            var distance = 0;
            var duration = 0;
            if (directionsDisplay.directions.routes[0].legs.length - 1 > 0) {
                for (var l = 0; l < directionsDisplay.directions.routes[0].legs.length; l++) {
                    for (var ws = 0; ws < directionsDisplay.directions.routes[0].legs[l].via_waypoints.length; ws++) {
                        w.push({
                            lat: directionsDisplay.directions.routes[0].legs[l].via_waypoints[ws].lat(),
                            lng: directionsDisplay.directions.routes[0].legs[l].via_waypoints[ws].lng()
                        });
                    }
                    if (l != directionsDisplay.directions.routes[0].legs.length - 1) {
                        w.push({
                            lat: directionsDisplay.directions.routes[0].legs[l].end_location.lat(),
                            lng: directionsDisplay.directions.routes[0].legs[l].end_location.lng(),
                        });
                    }
                    distance = distance + directionsDisplay.directions.routes[0].legs[l].distance.value;
                    duration = duration + directionsDisplay.directions.routes[0].legs[l].duration.value;
                }
                routeJSON.distance = distance;
                routeJSON.duration = duration;
            } else {
                waypoints = renderedRouteLegs.via_waypoints;
                for (var i = 0; i < waypoints.length; i++) {
                    w[i] =
                        {
                            lat: waypoints[i].lat(),
                            lng: waypoints[i].lng()
                        }
                }
                routeJSON.distance = renderedRouteLegs.distance.value;
                routeJSON.duration = renderedRouteLegs.duration.value;
            }

            routeJSON.waypoints = w;
            routeJSON.staticmap = dr.routes[0].overview_polyline;

            routelistJSON.push(routeJSON);
            $.ajax({
                url: modurl,
                type: 'post',
                data: {
                    name: document.getElementById("name").value,
                    description: document.getElementById("description").value,
                    googleroute: JSON.stringify(routelistJSON),
                    ispublished: document.getElementById("ispublished").checked,
                    authenticityToken: String(document.getElementsByName("authenticityToken")[0].value)
                },
                dataType: "json",
                success: (function (e) {
                    window.location.replace("/routes");
                }),
                error: (function (e) {
                    alert("Nem menthető! Töltsön ki minden mezőt!");
                })
            });
        }
    });
});


function reloadMap(e) {
    directionsDisplay = null;
    directionsService = null;
    startmarker = null;
    finishmarker = null;
    markerlist = [];
    waypoints = [];
    initMap();
}

