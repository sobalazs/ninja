package controllers;

import com.google.inject.Inject;
import dao.EtapDAO;
import dao.RouteDAO;
import dao.UserDAO;
import entity.Etap;
import entity.Route;
import entity.User;
import ninja.*;
import ninja.i18n.Messages;
import ninja.params.PathParam;
import ninja.session.FlashScope;
import utils.Path;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * Main controller for routes.
 * Controlls route create, modify, delete.
 */
public class RouteController {

    @Inject
    private RouteDAO routeDAO;

    @Inject
    private UserDAO userDAO;

    @Inject
    private EtapDAO etapDAO;

    @Inject
    private Messages msg;

    private Optional<String> language = Optional.of("hu");

    /**
     * Show all public routes.
     *
     * @param context All information from HTTP request.
     *
     * @return Template with all public routes.
     */
    public Result routes(Context context) {
        List<Route> routes = routeDAO.getAllRoutes();
        User user = userDAO.getUserByName(context.getSession().get("username"));

        return Results.html()
                .render("routes", routes)
                .render("user", user)
                .template(Path.Template.ROUTES);
    }

    /**
     * Validate event form and save event.
     *
     * @param mod     Control for route show and route modification.
     * @param id      Id from event to be saved.
     * @param context All information from HTTP request.
     * @return Template with all information from selected route.
     */
    public Result route(@PathParam("mod") String mod, @PathParam("id") Long id, Context context) {
        Route route = routeDAO.getRouteById(id);
        User user = userDAO.getUserByName(context.getSession().get("username"));

        if (mod == null) {
            //Return route information static.
            return Results.html()
                    .render("route", route)
                    .render("user", user)
                    .template(Path.Template.ROUTE);
        } else {
            //Return route information modifiable.
            return Results.html()
                    .render("route", route)
                    .render("user", user)
                    .template(Path.Template.MODROUTE);
        }
    }

    /**
     * Show template for route creation.
     *
     * @param context All information from HTTP request.
     * @return Template with map for route creation.
     */
    public Result newRouteTemp(Context context) {
        User user = userDAO.getUserByName(context.getSession().get("username"));
        return Results.html()
                .render("user", user)
                .template(Path.Template.NEWROUTE);
    }

    /**
     * Validate route form and save route.
     *
     * @param context All information from HTTP request.
     * @param fc      FlashScope object to give feedback message if error or success.
     * @return Empty JSON object or badRequest answer for javascript.
     */
    @FilterWith(AuthenticityFilter.class)
    public Result addRoute(Context context, FlashScope fc) {
        User user = userDAO.getUserByName(context.getSession().get("username"));
        HashMap<String, String> infoFromContext = extractFromContext(context);
        Boolean isPublished = Boolean.parseBoolean(context.getParameter("ispublished"));

        //Save route and throw success message.
        if (!infoFromContext.get("name").isEmpty() && !infoFromContext.get("description").isEmpty()) {
            if (routeDAO.addRoute(user.getId(), infoFromContext.get("name"), infoFromContext.get("description"), infoFromContext.get("jsonRoute"), isPublished)) {
                fc.success(msg.get("routesaved", language).get());
                return Results.json();
            }
        }

        return Results.badRequest();
    }

    /**
     * Validate route form and update route.
     *
     * @param id      Id from route that will be modified and saved.
     * @param context All information from HTTP request.
     * @param fc      FlashScope object to give feedback message if error or success.
     * @return Empty JSON object or badRequest answer for javascript.
     */
    @FilterWith(AuthenticityFilter.class)
    public Result modRoute(@PathParam("id") Long id, Context context, FlashScope fc) {
        User user = userDAO.getUserByName(context.getSession().get("username"));
        HashMap<String, String> infoFromContext = extractFromContext(context);

        Boolean isPublished = Boolean.parseBoolean(context.getParameter("ispublished"));

        //Save modified route and throw success message.
        if (!infoFromContext.get("name").isEmpty() && !infoFromContext.get("description").isEmpty()) {
            if (routeDAO.modRoute(user.getId(), id, infoFromContext.get("name"), infoFromContext.get("description"), infoFromContext.get("jsonRoute"), isPublished)) {
                fc.success(msg.get("routesaved", language).get());
                return Results.json();
            }
        }

        return Results.badRequest();
    }

    /**
     * Validate route form and update route.
     *
     * @param id      Id from route that will be deleted.
     * @param context All information from HTTP request.
     * @param fc      FlashScope object to give feedback message if error or success.
     * @return Empty JSON object or badRequest answer for javascript.
     */
    @FilterWith(AuthenticityFilter.class)
    public Result delRoute(@PathParam("id") Long id, Context context, FlashScope fc) {
        User user = userDAO.getUserByName(context.getSession().get("username"));
        List<Etap> etaps = etapDAO.getAllEtaps();

        if (routeDAO.delRoute(id, etaps)) {
            fc.success(msg.get("routedeleted", language).get());
        } else {
            fc.error(msg.get("routenotdeleted", language).get());
        }

        return Results.html()
                .render("profile", user)
                .redirect("/profile/" + user.getName())
                .template(Path.Template.LOGIN);

    }

    /**
     * Extract route information from context.
     *
     * @param context All information from HTTP request.
     * @return Hashmap with name, description and jsonRoute.
     */
    public HashMap<String, String> extractFromContext(Context context) {
        HashMap<String, String> infoForRoute = new HashMap<>();

        infoForRoute.put("name", context.getParameter("name"));
        infoForRoute.put("description", context.getParameter("description").replaceAll("(\r\n|\n)", "<br>"));
        infoForRoute.put("jsonRoute", context.getParameter("googleroute"));

        return infoForRoute;
    }
}
