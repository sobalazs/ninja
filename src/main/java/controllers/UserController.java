package controllers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import dao.EtapDAO;
import dao.EventDAO;
import dao.RouteDAO;
import dao.UserDAO;
import entity.Etap;
import entity.Event;
import entity.Route;
import entity.User;
import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.i18n.Messages;
import ninja.params.PathParam;
import ninja.session.FlashScope;
import ninja.session.Session;
import org.mindrot.jbcrypt.BCrypt;
import utils.Path;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Main controller for users.
 * Controlls user login and logout.
 */
@Singleton
public class UserController {

    @Inject
    private UserDAO userDAO;

    @Inject
    private EventDAO eventDAO;

    @Inject
    private RouteDAO routeDAO;

    @Inject
    private EtapDAO etapDAO;

    @Inject
    private Messages msg;

    private Optional<String> language = Optional.of("hu");

    /**
     * Returns a Template with all users from database.
     *
     * @param context All information from HTTP request.
     * @return Template with all users.
     */
    public Result users(Context context) {
        User user = userDAO.getUserByName(context.getSession().get("username"));
        List<User> users = userDAO.getAllUsers();

        return Results.html()
                .render("users", users)
                .render("user", user)
                .template(Path.Template.USERS);
    }

    /**
     * Returns a Template with logged in user informations.
     * (name, image, email, Subscribed Events and Routes)
     *
     * @param username Username from Session.
     * @param context  All information from HTTP request.
     * @return Template with specific user information.
     */
    public Result getProfileTemplate(@PathParam("username") String username, Context context) {
        User profile = userDAO.getUserByName(username);
        User user = userDAO.getUserByName(context.getSession().get("username"));

        List<Route> userroutes = routeDAO.getRoutesByUser(profile.getId());
        List<Event> events = new ArrayList<>();

        //Read all etaps where the user is subscribed to and create event-etap list.
        List<Etap> subbedetaps = null;
        if (profile != null) {
            subbedetaps = etapDAO.getEtapsForUser(profile.getId());
            for (Etap se : subbedetaps) {
                Event e = eventDAO.getEventByEtapId(se.getId());
                if (!events.contains(e)) {
                    events.add(e);
                }
            }
        }

        //List of etaos in subscribed events.
        List<Long> subbedto = new ArrayList<>();
        for (Etap etap : subbedetaps) {
            subbedto.add(etap.getId());
        }

        //return profile template
        return Results.html()
                .render("profile", profile)
                .render("user", user)
                .render("routes", userroutes)
                .render("events", events)
                .render("subbedto", subbedto)
                .template(Path.Template.PROFILE);
    }

    /**
     * Returns a Template for the login form.
     * (name, password)
     *
     * @return Template for login users.
     */
    public Result getLoginTemplate() {
        return Results.html()
                .template(Path.Template.LOGIN);
    }

    /**
     * Check user data and login user.
     *
     * @param session Session object where the username will be stored.
     * @param context All information from HTTP request.
     * @param fc      FlashScope object to give feedback message if error or success.
     * @return If user data ok then Main template if not then Login template.
     */
    public Result loginUser(Session session, Context context, FlashScope fc) {
        String username = context.getParameter("username");
        String passwd = context.getParameter("password");

        //No data given, throw error message.
        if (username.isEmpty() || passwd.isEmpty()) {
            fc.error(msg.get("nologinnameandpw", language).get());
            return Results.html()
                    .template(Path.Template.LOGIN);
        }

        //If no user found, trown error message, else check password.
        User user = userDAO.getUserByName(username);
        if (user == null) {
            fc.error(msg.get("nologinuser", language).get());
            return Results.html()
                    .template(Path.Template.LOGIN);
        } else {
            String hashedPassword = BCrypt.hashpw(passwd, user.getSalt());
            if (hashedPassword.equals(user.getHashpw())) {
                //If password ok, put username in session and login.
                session.put("username", username);
                fc.success(msg.get("succeslogin", language).get());
                return Results.html().redirect(Path.Web.MAIN)
                        .template(Path.Template.MAIN);
            }
        }

        //If Password is not correct, throw error message.
        fc.error(msg.get("notcorrectlogin", language).get());
        return Results.html()
                .template(Path.Template.LOGIN);
    }

    /**
     * Logout user and delete session data.
     *
     * @param session Session object for deleting user data by logout.
     * @return Login template.
     */
    public Result logoutUser(Session session) {
        session.remove("username");
        return Results.html()
                .template(Path.Template.LOGIN)
                .redirect((Path.Web.LOGIN));
    }

}
