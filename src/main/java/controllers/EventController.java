package controllers;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import dao.EtapDAO;
import dao.EventDAO;
import dao.RouteDAO;
import dao.UserDAO;
import entity.Etap;
import entity.Event;
import entity.Route;
import entity.User;
import ninja.*;
import ninja.i18n.Messages;
import ninja.params.PathParam;
import ninja.session.FlashScope;
import ninja.utils.NinjaProperties;
import utils.Path;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Main controller for events.
 * Controlls event create, delete, modigy and everything to do with events.
 */
@Singleton
public class EventController {

    @Inject
    private EventDAO eventDAO;

    @Inject
    private RouteDAO routeDAO;

    @Inject
    private UserDAO userDAO;

    @Inject
    private EtapDAO etapDAO;

    @Inject
    private NinjaProperties ninjaProperties;

    @Inject
    private Messages msg;

    private Optional<String> language = Optional.of("hu");

    /**
     * Shows all events sorted by date.
     *
     * @param context All information from HTTP request.
     * @return List with all events. If user is admin show also unpublished events.
     */
    public Result events(Context context) {
        List<Event> events = eventDAO.getAllEvents();

        events.sort(Comparator.comparing(Event::getDate));

        User user = userDAO.getUserByName(context.getSession().get("username"));
        return Results.html()
                .render("events", events)
                .render("user", user)
                .template(Path.Template.EVENTS);
    }

    /**
     * Show only requested event for modification or for display.
     *
     * @param mod     Control for event show and event modification.
     * @param id      Contains id from event.
     * @param context All information from HTTP request.
     * @return If mod is not null show modification template for event. When mod is null show event information static.
     */
    public Result event(@PathParam("mod") String mod, @PathParam("id") Long id, Context context) {
        Event event = eventDAO.getEventById(id);
        Set<Etap> etaps = new HashSet<>(event.getEtaps());

        User user = userDAO.getUserByName(context.getSession().get("username"));

        //Get all etaps where the user is subscribed to.
        List<Long> usersEtaps = etaps.stream()
                .filter(etap -> etap.getUsers().contains(user))
                .map(Etap::getId)
                .collect(Collectors.toList());

        //Get all users/etap.
        Map<String, String> usersByEtap = etaps.stream()
                .sorted(Comparator.comparing(Etap::getDt))
                .collect(Collectors.toMap(
                        etap -> etap.getId().toString(),
                        etap -> etap.getUsers().stream().map(User::getName).distinct().collect(Collectors.joining(", "))
                ));

        //Set etaps description new line character
        for (Etap etapitem : event.getEtaps()) {
            etapitem.getEtap().setDescription(etapitem.getEtap().getDescription().replace("<br>", "\n"));
        }

        if (mod == null) {
            return Results.html()
                    .render("etaps", etaps)
                    .render("event", event)
                    .render("user", user)
                    .render("subbedetaps", usersEtaps)
                    .render("subbedusers", usersByEtap)
                    .template(Path.Template.EVENT);
        } else {
            //Set event descriptions new line character
            event.setDescription(event.getDescription().replace("<br>", "\n"));

            List<Route> remainingRoutes = getRoutesNotInEvent(event);
            Date maxDate = getMaxDate();

            return Results.html()
                    .render("etaps", etaps)
                    .render("event", event)
                    .render("user", user)
                    .render("maxdate", maxDate)
                    .render("difference", remainingRoutes)
                    .template(Path.Template.MODEVENT);
        }
    }

    /**
     * Show form for new event.
     *
     * @param context All information from HTTP request.
     * @return Template for new event creation.
     */
    public Result newEvent(Context context) {
        User user = userDAO.getUserByName(context.getSession().get("username"));
        Date maxDate = getMaxDate();
        return Results.html()
                .render("user", user)
                .render("maxdate", maxDate)
                .template(Path.Template.NEWEVENT);
    }

    /**
     * Validate form data and if ok save event. Response with either with saved event or with error message.
     *
     * @param context All information from HTTP request.
     * @param fc      FlashScope object to give feedback message if error or success.
     * @return Template for modify event or if data is not valid, new event template.
     */
    @FilterWith(AuthenticityFilter.class)
    public Result addEvent(Context context, FlashScope fc) {
        User user = userDAO.getUserByName(context.getSession().get("username"));
        Date date = getDateFromContext(context, "yyyy-MM-dd");
        Date dt = getMaxDate();
        Boolean isPublished = getIsPublished(context);
        String name = context.getParameter("name");
        String desc = context.getParameter("description").replaceAll("(\r\n|\n)", "<br>");

        //Check date and if wrong throw error message.
        if (date.after(dt) || date.before(new Date())) {
            fc.error(msg.get("dateinfuture", language).get());
            return Results.html()
                    .render("name", name)
                    .render("desc", desc)
                    .render("user", user)
                    .render("maxdate", dt)
                    .template(Path.Template.NEWEVENT);
        }

        //If name is empty throw error message, else add event.
        if (name.equals("")) {
            fc.error(msg.get("fillallfield", language).get());
            return Results.html()
                    .render("desc", desc)
                    .render("user", user)
                    .render("maxdate", dt)
                    .template(Path.Template.NEWEVENT);
        } else {
            Long eventid = eventDAO.addEvent(name, desc, date, isPublished).getId();
            if (eventid != null) {
                fc.success(msg.get("eventcreated", language).get());
                return Results.html()
                        .redirect("/event/mod/" + eventid)
                        .template(Path.Template.MODEVENT);
            } else {
                fc.error(msg.get("eventerror", language).get());
            }
        }

        return Results.html()
                .render("user", user)
                .template(Path.Template.NEWEVENT);
    }

    /**
     * Validate event form and update event.
     *
     * @param context All information from HTTP request.
     * @param id      Id from event to be saved.
     * @param fc      FlashScope object to give feedback message if error or success.
     * @return If validation ok then event list when not ok modification template. Both with feedback message.
     */
    @FilterWith(AuthenticityFilter.class)
    public Result modEvent(Context context, @PathParam("id") Long id, FlashScope fc) {
        Boolean isPublished = getIsPublished(context);
        Date date = getDateFromContext(context, "yyyy-MM-dd");
        Date maxDate = getMaxDate();
        String name = context.getParameter("name");
        String description = context.getParameter("description").replaceAll("(\r\n|\n)", "<br>");

        //If name empty or date is outer limit return and throw error message.
        if (name.equals("") || date.after(maxDate) || date.before(new Date())) {
            fc.error(msg.get("fillallfield", language).get());
            return Results.html().redirect(id.toString());
        }

        //Save Event
        if (eventDAO.modEvent(name, description, date, id, isPublished)) {
            fc.success(msg.get("eventmodified", language).get());
        } else {
            fc.error(msg.get("eventnotmodified", language).get());
        }
        return Results.html()
                .redirect("/events")
                .template(Path.Template.EVENTS);
    }

    /**
     * Template for all routes that are not subscribed to event.
     *
     * @param id      Id from event where the new route will be added.
     * @param context All information from HTTP request.
     * @return List of all route that are not subscribed to event.
     */
    public Result addRouteToEventTemplate(@PathParam("id") Long id, Context context) {
        User user = userDAO.getUserByName(context.getSession().get("username"));
        Event event = eventDAO.getEventById(id);

        List<Route> remainingRoutes = getRoutesNotInEvent(event);

        return Results.html()
                .render("difference", remainingRoutes)
                .render("event", event)
                .render("user", user)
                .template((Path.Template.ADDROUTETOEVENT));
    }

    /**
     * Add selected route to event.
     *
     * @param id      Id from event where the new route will be added.
     * @param context All information from HTTP request.
     * @param fc      FlashScope object to give feedback message if error or success.
     * @return Template for modifying the event. Now with added route.
     */
    @FilterWith(AuthenticityFilter.class)
    public Result addRouteToEvent(@PathParam("id") Long id, Context context, FlashScope fc) {
        User user = userDAO.getUserByName(context.getSession().get("username"));
        Event event = eventDAO.getEventById(id);

        //If route selected add to event and throw succes message, else throw error message.
        if (context.getParameter("routeid") != null) {
            Long routeid = Long.parseLong(context.getParameter("routeid"));
            Date datetime = getDateFromContext(context, "yyyy.MM.dd.HH:mm");

            eventDAO.addEtap(event.getId(), routeid, datetime);

            Event e = eventDAO.getEventById(id);
            List<Etap> etaps = e.getEtaps();

            fc.success(msg.get("etapadded", language).get());
            return Results.html()
                    .redirect("/event/mod/" + e.getId())
                    .render("event", e)
                    .render("user", user)
                    .render("etaps", etaps);
        } else {
            fc.error(msg.get("etapnotselected", language).get());
            return addRouteToEventTemplate(id, context);
        }
    }

    /**
     * Removes selected route from event.
     *
     * @param id      Id from event where the route will be deleted.
     * @param context All information from HTTP request.
     * @param fc      FlashScope object to give feedback message if error or success.
     * @return Template for modifying the event. Now without deleted route.
     */
    @FilterWith(AuthenticityFilter.class)
    public Result delRouteFromEvent(@PathParam("id") Long id, Context context, FlashScope fc) {
        Event event = eventDAO.getEventById(id);
        Long etapid = Long.parseLong(context.getParameter("etapid"));
        User user = userDAO.getUserByName(context.getSession().get("username"));

        //Unsubscribe all user from etap
        for (User etapUser : etapDAO.getEtapById(etapid).getUsers()) {
            etapDAO.modEtap(etapUser, etapid);
        }

        //Remove etap and throw message.
        if (eventDAO.delEtap(event.getId(), etapid)) {
            fc.success(msg.get("etapdeleted", language).get());
        } else {
            fc.error(msg.get("etapnotdeleted", language).get());
        }

        event = eventDAO.getEventById(id);
        Set<Etap> etaps = new HashSet<>(event.getEtaps());

        //Prepare event description for html output.
        event.setDescription(event.getDescription().replace("<br>", "\n"));

        //Get all routes wich are not in event
        List<Route> routesInEvent = new ArrayList<>();
        for (Etap etap : etaps) {
            routesInEvent.add(etap.getEtap());
        }
        List<Route> routes = routeDAO.getAllRoutes();
        routes.removeAll(routesInEvent);
        routes = routes.stream().filter(r -> r.getIsactive().equals(true) && r.getIspublished().equals(true)).collect(Collectors.toList());

        return Results.html()
                .render("event", event)
                .render("user", user)
                .render("etaps", etaps)
                .render("maxdate", getMaxDate())
                .render("difference", routes)
                .template(Path.Template.MODEVENT);
    }

    /**
     * Subscribe user to event etap.
     *
     * @param id      Id from event.
     * @param context All information from HTTP request.
     * @return Template for event where the user is now subscribed to.
     */
    @FilterWith(AuthenticityFilter.class)
    public Result subToEvent(@PathParam("id") Long id, Context context) {
        Long etapid = Long.parseLong(context.getParameter("etapid"));
        User user = userDAO.getUserByName(context.getSession().get("username"));

        etapDAO.modEtap(user, etapid);

        Event event = eventDAO.getEventById(id);
        Set<Etap> etaps = new HashSet<>(event.getEtaps());

        return Results.html()
                .redirect("/event/" + event.getId())
                .render("etaps", etaps)
                .render("event", event)
                .render("user", user)
                .template(Path.Template.EVENT);
    }

    /**
     * Delete event and return success or error message.
     *
     * @param id Id from event to be deleted.
     * @param fc FlashScope object to give feedback message if error or success.
     * @return Template with all remained events.
     */
    public Result delEvent(@PathParam("id") Long id, FlashScope fc) {
        if (eventDAO.delEvent(id)) {
            fc.success(msg.get("eventdeleted", language).get());
            return Results.html()
                    .redirect("/events")
                    .template(Path.Template.EVENTS);
        } else {
            fc.error(msg.get("cantdelete", language).get());
            return Results.html()
                    .redirect("/events")
                    .template(Path.Template.EVENTS);
        }
    }

    /**
     * Calculate maximum date for events.
     *
     * @return Maximum date when the event can be saved.
     */
    private Date getMaxDate() {
        Integer limit = Integer.parseInt(ninjaProperties.get("eventDateLimit"));
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, limit);

        return c.getTime();
    }

    /**
     * Read date from context and create Date object.
     *
     * @param context All information from HTTP request.
     * @param pattern Id from event to be deleted.
     * @return Event date object.
     */
    private Date getDateFromContext(Context context, String pattern) {
        Date date = null;

        SimpleDateFormat df = new SimpleDateFormat(pattern);
        try {
            if (pattern.contains("HH:mm")) {
                date = df.parse(context.getParameter("date") + context.getParameter("time"));
            } else {
                date = df.parse(context.getParameter("date"));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    /**
     * Convert html checkbox to Boolean value.
     *
     * @param context All information from HTTP request.
     * @return True if event is published and false when not.
     */
    private Boolean getIsPublished(Context context) {
        if (context.getParameter("ispublished") != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Reads all route from DB that are not in specified event.
     *
     * @param event Event for searching routes that are not in this event.
     * @return List of route that are not in event.
     */
    private List<Route> getRoutesNotInEvent(Event event) {
        List<Etap> eventEtaps = event.getEtaps();
        List<Route> routesInEvent = new ArrayList<>();
        List<Route> routes = routeDAO.getAllRoutes();

        for (Etap etap : eventEtaps) {
            routesInEvent.add(etap.getEtap());
        }
        routes.removeAll(routesInEvent);

        return routes.stream().filter(r -> r.getIsactive().equals(true) && r.getIspublished().equals(true)).collect(Collectors.toList());
    }
}
