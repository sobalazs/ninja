package utils;

/**
 * Define all routes for request and response.
 */
public class Path {
    /**
     * Define all routes for request.
     */
    public class Web {
        public static final String MAIN = "/";
        public static final String LOGIN = "/login";
        public static final String LOGOUT = "/logout";

        public static final String EVENTS = "/events";
        public static final String EVENT = "/event/{id: [0-9]+}";
        public static final String NEWEVENT = "/event/new";
        public static final String MODEVENT = "/event/{mod: mod}/{id: [0-9]+}";
        public static final String DELEVENT = "/delevent/{id: [0-9]+}";
        public static final String ADDROUTETOEVENT = "/event/{add: addroute}/{id: [0-9]+}";
        public static final String DELROUTEFROMEVENT = "/event/{del: deletap}/{id: [0-9]+}";

        public static final String ROUTES = "/routes";
        public static final String ROUTE = "/route/{id: [0-9]+}";
        public static final String NEWROUTE = "/route/new";
        public static final String MODROUTE = "/route/{mod: mod}/{id: [0-9]+}";
        public static final String DELROUTE = "/route/del/{id: [0-9]+}";

        public static final String USERS = "/users";
        public static final String PROFILE = "/profile/{username: [a-zA-Z0-9]+}";
        public static final String SUBSCRIBETOEVENT = "/user/addevent/{id: [0-9]+}";
    }

    /**
     * Define all routes for response.
     */
    public class Template {
        public static final String MAIN = "views/ApplicationController/index.ftl.html";
        public static final String LOGIN = "views/UserController/login.ftl.html";

        public static final String EVENTS = "views/EventController/events.ftl.html";
        public static final String EVENT = "views/EventController/event.ftl.html";
        public static final String NEWEVENT = "views/EventController/newEvent.ftl.html";
        public static final String MODEVENT = "views/EventController/modEvent.ftl.html";
        public static final String ADDROUTETOEVENT = "views/EventController/addRouteToEvent.ftl.html";

        public static final String ROUTES = "views/RouteController/routes.ftl.html";
        public static final String ROUTE = "views/RouteController/route.ftl.html";
        public static final String NEWROUTE = "views/RouteController/newRoute.ftl.html";
        public static final String MODROUTE = "views/RouteController/modRoute.ftl.html";

        public static final String USERS = "views/UserController/users.ftl.html";
        public static final String PROFILE = "views/UserController/profile.ftl.html";
    }
}
