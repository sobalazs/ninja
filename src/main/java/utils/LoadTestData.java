package utils;

import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import entity.User;
import org.apache.commons.io.IOUtils;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.List;

/**
 * Controlls data to load into DB from JSON.
 */
public class LoadTestData {

    @Inject
    private Provider<EntityManager> entityManagerProvider;

    @Inject
    private GsonConverter rc;

    /**
     * Read JSON file and add Users to DB.
     */
    @Transactional
    public void addTestUsers() {
        EntityManager em = entityManagerProvider.get();
        try {
            //Read testuser data from JSON file.
            InputStream resource = getClass().getResourceAsStream("/userTest.json");
            String jsonString = IOUtils.toString(resource, Charset.defaultCharset());

            //Convert JSON to User objects.
            List<User> users = rc.convertUsersFromJson(jsonString);

            //Load all users to DB.
            for (User u : users) {
                em.persist(u);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}