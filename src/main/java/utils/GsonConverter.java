package utils;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import entity.Route;
import entity.User;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Convert given json String to Java Object.
 */
public class GsonConverter {

    /**
     * Read JSON and convert to Route object.
     *
     * @param jsonRoute JSON with route informations.
     * @return List of Routes
     */
    public List<Route> convertRouteFromJson(String jsonRoute) {
        Gson g = new Gson();
        Type collectionType = new TypeToken<List<Route>>() {
        }.getType();
        return g.fromJson(jsonRoute, collectionType);
    }

    /**
     * Read JSON and convert to User object.
     *
     * @param jsonUsers JSON with user informations.
     * @return List of Users
     */
    public List<User> convertUsersFromJson(String jsonUsers) {
        Gson g = new Gson();
        Type collectionType = new TypeToken<List<User>>() {
        }.getType();
        return g.fromJson(jsonUsers, collectionType);
    }
}