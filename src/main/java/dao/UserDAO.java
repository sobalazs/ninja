package dao;

import com.google.inject.Inject;
import entity.User;
import ninja.jpa.UnitOfWork;

import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class UserDAO {

    @Inject
    private Provider<EntityManager> entityManagerProvider;

    @UnitOfWork
    public List<User> getAllUsers() {
        EntityManager em = entityManagerProvider.get();

        TypedQuery<User> query = em.createQuery("SELECT x FROM User x", User.class);
        List<User> users = query.getResultList();

        return users;
    }

    @UnitOfWork
    public User getUserByName(String username) {
        EntityManager em = entityManagerProvider.get();

        TypedQuery<User> query = em.createQuery("SELECT u FROM User u", User.class);
        List<User> users = query.getResultList();

        return users.stream().filter(u -> u.getName().equals(username)).findFirst().orElse(null);
    }
}