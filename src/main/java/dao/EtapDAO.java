package dao;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import entity.Etap;
import entity.User;
import ninja.jpa.UnitOfWork;

import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.stream.Collectors;

public class EtapDAO {

    @Inject
    private Provider<EntityManager> entityManagerProvider;

    @UnitOfWork
    public Etap getEtapById(Long id) {
        EntityManager em = entityManagerProvider.get();
        return em.find(Etap.class, id);
    }

    @UnitOfWork
    public List<Etap> getAllEtaps() {
        EntityManager em = entityManagerProvider.get();
        TypedQuery<Etap> query = em.createQuery("SELECT x FROM Etap x", Etap.class);

        return query.getResultList();
    }

    @Transactional
    public Boolean modEtap(User user, Long etapid) {
        EntityManager em = entityManagerProvider.get();
        Etap etap = em.find(Etap.class, etapid);
        List<User> users = etap.getUsers().stream().distinct().collect(Collectors.toList());
        if (users.contains(user)) {
            users.remove(user);
            etap.setUsers(users);
        } else {
            etap.addUser(user);
        }
        return true;
    }

    @UnitOfWork
    public List<Etap> getEtapsForUser(Long userid) {
        EntityManager em = entityManagerProvider.get();

        TypedQuery<Etap> query = em
                .createQuery("SELECT etap FROM Etap etap JOIN etap.users user WHERE user.id IN :userid", Etap.class);
        query.setParameter("userid", userid);

        return query.getResultList();
    }

    @UnitOfWork
    public List<String> getUserNamesFromEtap(Long etapid) {
        EntityManager em = entityManagerProvider.get();
        Etap e = em.find(Etap.class, etapid);

        return e.getUsers().stream().map(User::getName).collect(Collectors.toList());
    }
}