package dao;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import entity.Etap;
import entity.Route;
import entity.User;
import ninja.jpa.UnitOfWork;
import utils.GsonConverter;

import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class RouteDAO {

    @Inject
    private Provider<EntityManager> entityManagerProvider;

    @Inject
    private GsonConverter gc;

    @UnitOfWork
    public List<Route> getAllRoutes() {
        EntityManager em = entityManagerProvider.get();

        TypedQuery<Route> query = em.createQuery("SELECT x FROM Route x", Route.class);
        List<Route> routes = query.getResultList();

        return routes;
    }

    @UnitOfWork
    public Route getRouteById(Long id) {
        EntityManager em = entityManagerProvider.get();
        Route r = em.find(Route.class, id);
        return r;
    }

    @Transactional
    public Boolean addRoute(Long userid, String name, String description, String jsonroute, Boolean isPublished) {
        EntityManager em = entityManagerProvider.get();

        Route route = gc.convertRouteFromJson(jsonroute).get(0);
        User user = em.find(User.class, userid);
        route.setOwner(user);
        route.setName(name);
        route.setDescription(description);
        route.setIspublished(isPublished);
        route.setIsactive(true);
        try {
            em.persist(route);
            return true;
        } catch (Exception ex) {
            System.out.println("Hiba: " + ex);
        }
        return false;
    }

    @UnitOfWork
    public List<Route> getRoutesByUser(Long id) {
        EntityManager em = entityManagerProvider.get();
        TypedQuery<Route> query = em
                .createQuery("SELECT route FROM Route route INNER JOIN route.owner owner WHERE owner.id IN :userid", Route.class);
        query.setParameter("userid", id);

        return query.getResultList();
    }

    @Transactional
    public Boolean delRoute(Long routeid, List<Etap> etaps) {
        EntityManager em = entityManagerProvider.get();

        try {
            if (etaps.size() > 0) {
                etaps.stream()
                        .forEach(etap -> {
                            if (etap.getEtap().getId().equals(routeid) && etap.getEtap().getIsactive().equals(true)) {
                                etap.getEtap().setIsactive(false);
                                em.merge(etap);
                            } else {
                                Route r = em.find(Route.class, routeid);
                                if (r != null) {
                                    r.setIsactive(false);
                                }

                            }
                        });
            } else {
                em.remove(em.find(Route.class, routeid));
            }
            return true;
        } catch (Exception ex) {
            System.out.println("Hiba: " + ex);
            return false;
        }
    }

    @Transactional
    public Boolean modRoute(Long userid, Long id, String name, String description, String jsonroute, Boolean ispublished) {
        EntityManager em = entityManagerProvider.get();
        Route r = em.find(Route.class, id);
        try {
            Route route = gc.convertRouteFromJson(jsonroute).get(0);
            User user = em.find(User.class, userid);

            r.setOwner(user);
            r.setIsactive(true);
            r.setDestination(route.getDestination());
            r.setStart(route.getStart());
            r.setDistance(route.getDistance());
            r.setDuration(route.getDuration());
            r.setStaticmap(route.getStaticmap());
            r.setName(name);
            r.setDescription(description);
            r.setIspublished(ispublished);
            r.setWaypoints(route.getWaypoints());

            em.merge(r);
            return true;
        } catch (Exception ex) {
            System.out.println("Hiba: " + ex);
            return false;
        }
    }
}