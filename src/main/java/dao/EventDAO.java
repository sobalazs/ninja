package dao;

import com.google.inject.Inject;
import com.google.inject.persist.Transactional;
import entity.Etap;
import entity.Event;
import entity.Route;
import entity.User;
import ninja.jpa.UnitOfWork;

import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class EventDAO {

    @Inject
    private Provider<EntityManager> entityManagerProvider;

    @Inject
    private RouteDAO routeDAO;

    @UnitOfWork
    public List<Event> getAllEvents() {
        EntityManager em = entityManagerProvider.get();

        TypedQuery<Event> query = em.createQuery("SELECT x FROM Event x", Event.class);
        List<Event> events = query.getResultList();

        return events;
    }

    @UnitOfWork
    public Event getEventById(Long id) {
        EntityManager em = entityManagerProvider.get();
        Event e = em.find(Event.class, id);
        return e;
    }

    @Transactional
    public Event addEvent(String name, String desc, Date date, Boolean ispublished) {
        EntityManager em = entityManagerProvider.get();
        Event e = new Event();
        e.setName(name);
        e.setDescription(desc);
        e.setDate(date);
        e.setIspublished(ispublished);
        try {
            em.persist(e);
            return e;
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
    }

    @Transactional
    public Boolean modEvent(String name, String desc, Date date, Long id, Boolean ispublished) {
        EntityManager em = entityManagerProvider.get();
        Event e = em.find(Event.class, id);

        e.setName(name);
        e.setDescription(desc);
        e.setDate(date);
        e.setIspublished(ispublished);

        em.merge(e);
        return true;
    }

    @Transactional
    public void addEtap(Long eventid, Long routeid, Date datetime) {
        EntityManager em = entityManagerProvider.get();
        Event e = em.find(Event.class, eventid);
        Route r = em.find(Route.class, routeid);
        List<Etap> etaps = e.getEtaps().stream().distinct().collect(Collectors.toList());

        Etap etap = new Etap();
        etap.setDt(datetime);
        etap.setEtap(r);
        etaps.add(etap);
        e.setEtaps(etaps);
    }

    @UnitOfWork
    public Event getEventByEtapId(Long etapid) {
        EntityManager em = entityManagerProvider.get();
        TypedQuery<Event> query = em
                .createQuery("SELECT event FROM Event event INNER JOIN event.etaps etap WHERE etap.id IN :etapid", Event.class);
        query.setParameter("etapid", etapid);

        return query.getSingleResult();
    }

    @Transactional
    public Boolean delEvent(Long id) {
        EntityManager em = entityManagerProvider.get();
        try {
            em.remove(em.find(Event.class, id));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Transactional
    public Boolean delEtap(Long eventid, Long etapid) {
        EntityManager em = entityManagerProvider.get();
        Event e = em.find(Event.class, eventid);
        Etap etap = em.find(Etap.class, etapid);

        List<User> users = etap.getUsers();
        etap.setUsers(new ArrayList<>());

        em.merge(etap);

        List<Etap> etaps = e.getEtaps().stream().distinct().collect(Collectors.toList());
        etaps.remove(etap);
        e.setEtaps(etaps);
        return true;
    }
}
