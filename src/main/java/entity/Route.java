package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "ROUTE")
public class Route implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "routeid")
    private Long id;

    @OneToOne
    @JoinColumn
    private User owner;

    @Column
    private String name;

    @Column(length = 1500)
    private String description;

    @Column
    private Boolean ispublished;

    @Column
    private Boolean isactive;

    @Column(length = 2000)
    private String staticmap;

    @Column
    private Long distance;

    @Column
    private Long duration;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Place start;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Place destination;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.MERGE})
    @JoinColumn
    private List<Place> waypoints;

    public Route() {
    }

    public Route(User owner, String name, String description, Boolean ispublished, Boolean isactive, String staticmap, Long distance, Long duration, Place start, Place destination, List<Place> waypoints) {
        this.owner = owner;
        this.name = name;
        this.description = description;
        this.ispublished = ispublished;
        this.isactive = isactive;
        this.staticmap = staticmap;
        this.distance = distance;
        this.duration = duration;
        this.start = start;
        this.destination = destination;
        this.waypoints = waypoints;
    }

    public Long getId() {
        return id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String desc) {
        this.description = desc;
    }

    public Boolean getIspublished() {
        return ispublished;
    }

    public void setIspublished(Boolean ispublished) {
        this.ispublished = ispublished;
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public Place getStart() {
        return start;
    }

    public void setStart(Place start) {
        this.start = start;
    }

    public String getStaticmap() {
        return staticmap;
    }

    public void setStaticmap(String staticmap) {
        this.staticmap = staticmap;
    }

    public Long getDistance() {
        return distance;
    }

    public void setDistance(Long distance) {
        this.distance = distance;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Place getDestination() {
        return destination;
    }

    public void setDestination(Place destination) {
        this.destination = destination;
    }

    public List<Place> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<Place> waypoints) {
        this.waypoints = waypoints;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Route route = (Route) o;

        return id.equals(route.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", owner='" + owner + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", staticmap='" + staticmap + '\'' +
                ", start=" + start +
                ", destination=" + destination +
                ", waypoints=" + waypoints +
                '}';
    }
}
