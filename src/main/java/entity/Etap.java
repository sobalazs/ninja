package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "ETAP")
public class Etap implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "etapid")
    private Long id;

    @Column(name = "etapdatetime")
    private Date dt;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "etap")
    private Route etap;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(joinColumns = @JoinColumn(name = "ETAP_ID"), inverseJoinColumns = @JoinColumn(name = "USER_ID"))
    private List<User> users;

    public Etap() {
    }

    public Etap(Date dt, Route etap) {
        this.dt = dt;
        this.etap = etap;
    }

    public Long getId() {
        return id;
    }

    public Date getDt() {
        return dt;
    }

    public void setDt(Date dt) {
        this.dt = dt;
    }

    public Route getEtap() {
        return etap;
    }

    public void setEtap(Route etap) {
        this.etap = etap;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void addUser(User user) {
        this.users.add(user);
    }

    public void removeUser(User user) {
        this.users.remove(user);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Etap etap = (Etap) o;

        return id.equals(etap.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Etap{" +
                "id=" + id +
                ", dt=" + dt +
                ", etap=" + etap +
                '}';
    }
}
