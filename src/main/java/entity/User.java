package entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "USER")
public class User implements Serializable {

    @Id
    @GeneratedValue
    @Column(name = "userid")
    private Long id;

    @Column
    private String name;

    @Column
    private String imglink;

    @Column
    private String salt;

    @Column
    private String hashpw;

    @Column
    private String email;

    @Column
    private Boolean isadmin;

    public User() {
    }

    public User(String name, String salt, String hashpw, String email, Boolean isadmin) {
        this.name = name;
        this.salt = salt;
        this.hashpw = hashpw;
        this.email = email;
        this.isadmin = isadmin;
    }

    public Long getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImglink() {
        return imglink;
    }

    public void setImglink(String imglink) {
        this.imglink = imglink;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getHashpw() {
        return hashpw;
    }

    public void setHashpw(String hashpw) {
        this.hashpw = hashpw;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsadmin() {
        return isadmin;
    }

    public void setIsadmin(Boolean isadmin) {
        this.isadmin = isadmin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salt='" + salt + '\'' +
                ", hashpw='" + hashpw + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
