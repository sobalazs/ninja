package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "EVENT")
public class Event implements Serializable, Comparable<Event> {

    @Id
    @GeneratedValue
    @Column(name = "eventid")
    private Long id;

    @Column
    private String name;

    @Column(length = 1500)
    private String description;

    @Temporal(TemporalType.DATE)
    @Column
    private Date date;

    @Column
    private Boolean ispublished;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "EVENT_ETAP", joinColumns = @JoinColumn(name = "EVENT_ID"), inverseJoinColumns = @JoinColumn(name = "ETAP_ID"))
    private List<Etap> etaps;

    public Event() {
    }

    public Event(String name, String description, Date date, Boolean ispublished) {
        this.name = name;
        this.description = description;
        this.date = date;
        this.ispublished = ispublished;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getIspublished() {
        return ispublished;
    }

    public void setIspublished(Boolean ispublished) {
        this.ispublished = ispublished;
    }

    public List<Etap> getEtaps() {
        return etaps;
    }

    public void setEtaps(List<Etap> etaps) {
        this.etaps = etaps;
    }

    public void addEtap(Etap etap) {
        this.etaps.add(etap);
    }

    public void delEtap(Etap etap) {
        this.etaps.remove(etap);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        return id.equals(event.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", date=" + date +
                ", etaps=" + etaps +
                '}';
    }

    @Override
    public int compareTo(Event o) {
        return this.getDate().compareTo(o.getDate());
    }
}