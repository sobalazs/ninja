package controllers;

import ninja.NinjaTest;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UserControllerNinjaTest extends NinjaTest {

    @Test
    public void testGetIndexTemplate() {
        String result = ninjaTestBrowser.makeRequest(getServerAddress() + "/");
        assertTrue(result.contains("Üdvözöllek"));
    }

    @Test
    public void testGetUsersTemplate(){
        String result = ninjaTestBrowser.makeRequest(getServerAddress() + "/users");
        assertFalse(result.contains("admin"));
        assertFalse(result.contains("user1"));
        assertFalse(result.contains("user2"));
    }

}