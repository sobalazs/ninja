package controllers;

import ninja.NinjaFluentLeniumTest;
import org.junit.Test;

import static com.thoughtworks.selenium.SeleneseTestBase.assertTrue;

public class UserControllerFluentLeniumTest extends NinjaFluentLeniumTest {

    @Test
    public void testGetLoginTemplate() {
        goTo(getServerAddress() + "/");
        assertTrue(title().contains("Túraszervezés"));
        goTo(getServerAddress() + "/login");
        click("#login");
        assertTrue(pageSource().contains("alert-danger"));
    }

}